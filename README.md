# gitlab server local

## Oracle VirtualBox with Ubuntu server 64b
* image: https://www.osboxes.org/ubuntu-server/
* login/pwd: osboxes/osboxes.org
* es keyboad: https://www.redeszone.net/tutoriales/servidores/cambiar-idioma-teclado-ubuntu-server/
* network, use network type: "Bridged Adapter"

## Install gitlab
* follow: https://about.gitlab.com/install/#ubuntu
* if there are issues with sshd service do run ```sudo ssh-keygen -A``` and then reboot
* connect from os x terminal: ```ssh osboxes@192.168.1.141```

IMPOSSIBLE TO SET UP A LOCAL GITLAB INSTANCE having too many issues


# gitlab server Cloud
##  trial gitserver: https://gitlab.com/testing_lfs/blobs
* branches: master (11.5), 11.3 and 11.4

### Moving git blobs to git LFS
* In origin side git LFS should be configured: https://docs.gitlab.com/ee/administration/lfs/index.html


#### Recipe
Based on: https://docs.gitlab.com/ee/topics/git/lfs/migrate_to_git_lfs.html
* Install bfg: ```brew install bfg```
* Install git-lfs: ```brew install git-lfs```
* Clone repo: ```git clone --mirror https://gitlab.com/testing_lfs/blobs.git```
* Convert the Git history with BFG: ```bfg --convert-to-git-lfs "*.mp3" --no-blob-protection blobs.git```
* Clean up the repository: 
```
    cd blobs.git
    git reflog expire --expire=now --all && git gc --prune=now --aggressive
```
* Install Git LFS in the mirror repository: ```git lfs install```
* Unprotect the default branch, so that we can force-push the rewritten repository (from the gitlab web)
* Force-push to GitLab: ```git push --force``` **IT SPREADS ALL LFS MIGRATION TO ALL BRANCHES**

**RECOMMENDATION** for all repository users: Do a new repository CLONE in order to avoid weird merging issues.

**NOTICE** The repository size does not decrease, however working with file pointers is more agile than working with entire files.





